<?php

/* ******************** START SETTINGS ******************** */

/* DOC ROOT SETTINGS */

// docroot: The path you extracted this repo to
// fileroot: The start of the backup filename
// filepath: Location of mysql backups (shouldn't need to edit)

$docroot		= "";	
$fileroot 		= "";
$filepath		= $docroot . 'mysql_backups/';

/* MySQL SETTINGS */

// mysqluser: MySQL Username
// mysqlpass: MySQL Password
// mysqlhost: MySQL Hostname
// mysqldb: MySql Database To Backup
// mysqldumppath: Location of 'mysqldump'

$mysqluser 		= "";
$mysqlpass 		= "";
$mysqlhost 		= "";
$mysqldb		= "";
$mysqldumppath 	= "";

/* AMAZON S3 SETTINGS */

// s3bucket: S3 Bucket Name
// s3folder: S3 subfolder for backups
// s3location: Folder for s3cmd (shouldn't need to edit)

$s3bucket		= "";
$s3folder		= "";
$s3location		= $docroot . 's3cmd/';

/* EMAIL SETTINGS */

// swift_required: Location of swift required library file (shouldn't need to edit)
// mandrill_user: Mandrill username
// mandrill_pass: Mandrill API Key
// from_email: From Email
// from_name: Who the email is from

$swift_required = $docroot . 'swift/lib/swift_required.php';
$mandrill_user	= "";
$mandrill_pass	= "";
$from_email		= "";
$from_name 		= "";

/* EMAIL CONTENT */

$subject 		= "";
$text			= "";
$html			= "";

/* OTHER */

// daystokeep: Number of days to keep local backups
// findlocation: path for 'find' (probably do not need to edit)
$daystokeep		= 14;
$findlocation	= "/usr/bin/find";

/* ******************** END SETTINGS ******************** */