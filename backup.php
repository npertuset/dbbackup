#!/usr/bin/php
<?php
/*
* dbBackup: Ability to backup a database and push it to amazon S3, email it, or keep it locally. You can set the number of backups to keep locally as well.
*
* Examples:
* 	./backup --s3=yes --email=nathanpertuset@hotmail.com
* 	./backup --email=nathanpertuset@hotmail.com
* 	./backup --s3=yes
*
* Installation:
*	1. Unzip outside of documentroot
* 	2. Set each config setting below
*	3. Setup S3
* 	4. Setup Mandrill
*	5. Set Cronjobs:
*		a. Daily Backup With S3 Sync (Daily @ 10pm): 0 22 * * * /path/to/backup --s3=yes --email=email@email.com
*		b. Email Once Per Week (Saturday @ 8am): 0 8 * * 6 /path/to/backup --s3=yes --email=email@email.com
*
* S3 Setup:
*	1. Make sure access code has been set in s3cmd/s3config (only file needing to edit)
*	2. Create bucket (and set variable)
*	3. Create 'mysql_backups' folder (and set variable)
* 	4. Create 'Lifestyle' to delete files after X days
*
* Mandrill Setup
*	1. Create Account
*	2. Get API Key
*
* Additional Options:
*	- You can call this from a php script (to backup and email)
*/

include "config.php";

// DO NOT EDIT BELOW HERE:
date_default_timezone_set('America/Los_Angeles');

$send_s3 = false;
$send_email = false;
$to_email = "";

// UPDATE SOON
// Parse command line arguments for --s3 or --email
// If they are set, set the decision variables to true

foreach( $argv as $argument ) {
    if( $argument == $argv[ 0 ] ) continue;

    $pair = explode( "=", $argument );
    $variableName = substr( $pair[ 0 ], 2 );
    $variableValue = $pair[ 1 ];
    
    if($variableName == 's3' && $variableValue == true) {
        $send_s3 = true;
    }
    
    if($variableName == 'email' && filter_var($variableValue, FILTER_VALIDATE_EMAIL)) {
        $send_email = true;
        $to_email = $variableValue;
    }
}

// Save the backup
exec($mysqldumppath . "mysqldump --host=" . $mysqlhost . " --quick --user=" . $mysqluser . " --password=" . $mysqlpass . " " . $mysqldb . " > " . $filepath . $fileroot . ".sql");

// Compress the SQL file
$backup_name = $fileroot . "." . date('m') . "." . date('d') . "." . date('Y') . ".tar.gz";

exec("tar czf " . $filepath . $backup_name . " -C " . $filepath . " " . $fileroot . ".sql");

// Can send to S3? 
if($send_s3 == true) {
	exec($s3location . "s3cmd --config=" . $s3location . "s3config put -f " . $filepath . $backup_name . " s3://" . $s3bucket . "/" . $s3folder . "/");
}

// Email? 
if($send_email == true) {
	include_once $swift_required;

	$from = array($from_email => $from_name);
	
	$to = array($to_email);
	
	$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
	$transport->setUsername($mandrill_user);
	$transport->setPassword($mandrill_pass);
	$swift = Swift_Mailer::newInstance($transport);
	
	$message = new Swift_Message($subject);
	$message->setFrom($from);
	$message->setBody($html, 'text/html');
	$message->setTo($to);
	$message->addPart($text, 'text/plain');
	
	$message->attach(
		Swift_Attachment::fromPath( $filepath . $backup_name )->setFilename($backup_name)
	);

	if ($recipients = $swift->send($message, $failures)) {
		//print_r($recipients);
		//print_r($failures);
		//echo 'Message successfully sent!';
	} else {
		//echo "There was an error:\n";
		//print_r($failures);
	}
}

// Erase old files:
exec($findlocation . " " . $filepath . " -name '*" . $fileroot . "*' -type f -mtime +" . $daystokeep . " -exec rm -rf {} \;");

// Remove the SQL file
//echo "Removing files...\n";
exec("rm " . $filepath . $fileroot . ".sql");

?>